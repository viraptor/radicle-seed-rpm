%global commit 529d797d085a8e46efd5e6a56c35363626174f4f
%global shortcommit %(c=%{commit}; echo ${c:0:7})
%global commitdate 20210902
%global debug_package %{nil}

Name:           radicle-seed
Epoch:          1
Version:        0
Release:        11.%{commitdate}git%{shortcommit}%{?dist}
Summary:        Radicle seed

License:        GPLv3
URL:            https://radicle.xyz
Source0:        https://github.com/radicle-dev/radicle-bins/archive/%{commit}.zip
Source1:        %{name}.service
Source2:        %{name}.defaults
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires(pre):  shadow-utils

Requires:       git

BuildRequires:  systemd-rpm-macros
BuildRequires:  gcc nodejs
%if 08 != 0%{?centos}
BuildRequires:  yarnpkg
%endif

%description
Radicle seed binaries and website static files


%prep
%setup -q -n radicle-bins-%{commit}
curl https://sh.rustup.rs -sSf | sh -s -- -y --profile minimal --default-toolchain $(cat rust-toolchain)

%build
%if 08 == 0%{?centos}
npm install yarn
(cd seed/ui && ../../node_modules/.bin/yarnpkg && ../../node_modules/.bin/yarnpkg build)
%else
(cd seed/ui && yarnpkg && yarnpkg build)
%endif
~/.cargo/bin/cargo build --release

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
install -m 0755 target/release/radicle-seed-node %{buildroot}%{_bindir}/radicle-seed-node
install -m 0755 target/release/radicle-keyutil %{buildroot}%{_bindir}/radicle-keyutil
install -d -m 0755 %{buildroot}%{_datadir}/%{name}
cp -r seed/ui/public %{buildroot}%{_datadir}/%{name}
install -D -m 0644 %{SOURCE1} %{buildroot}/usr/lib/systemd/system/%{name}.service
install -D -m 0644 %{SOURCE2} %{buildroot}%{_sysconfdir}/default/%{name}
install -d -m 0755 %{buildroot}/var/lib/%{name}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/radicle-seed-node
%{_bindir}/radicle-keyutil
%{_datadir}/%{name}
%config %{_sysconfdir}/default/%{name}
/usr/lib/systemd/system/%{name}.service
%attr(0755, radicle, radicle) %dir /var/lib/%{name}
%doc README.md
%license LICENSE

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%pre
getent group radicle >/dev/null || groupadd -r radicle
getent passwd radicle >/dev/null || \
    useradd -r -g radicle -d /var/lib/%{name} -s /sbin/nologin \
    -c "Radicle seed service owner" radicle
exit 0

%changelog
* Wed May 19 2021 Vi <radicle@viraptor.info> - 0.0.0-4
- initial

